import ezFetch from 'ez-fetch'

const API_URL = 'http://127.0.0.1:8000/api/configuration/residency/';
// const API_URL = 'http://10.80.90.173:7001/api/configuration/residency';

export default {
  getResidencia(){
    return ezFetch.get(API_URL)
  },
  getResidenciaView(id){
    return ezFetch.get(`${API_URL}/${id}`)
  }
}
