import Vue from 'vue'
import Router from 'vue-router'
import Landing from '@/components/Landing'
import Residencia from '@/components/configuracion/Residencia'
import ResidenciaView from '@/components/configuracion/Views/ResidenciaView'
import ResidenciaForm from '@/components/configuracion/Forms/ResidenciaForm'
import ResidenciaCrud from '@/components/configuracion/Cruds/ResidenciaCrud'
import HabitacionCrud from '@/components/configuracion/Cruds/HabitacionCrud'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Landing',
      component: Landing
    },
    {
      path: '/residencia',
      name: 'Residencia',
      component: Residencia
    },
    {
      path: '/residencia/:id',
      name: 'ResidenciaView',
      component: ResidenciaView
    },
    {
      path: '/residencia/new',
      name: 'ResidenciaForm',
      component: ResidenciaForm
    },
    {
      path: '/residenciacrud',
      name: 'ResidenciaCrud',
      component: ResidenciaCrud
    },
    {
      path: '/habitacioncrud',
      name: 'HabitacionCrud',
      component: HabitacionCrud
    },
  ]
})
